from django.forms import ModelForm
import models as M

class ShitHappens(ModelForm):
    class Meta:
        model = M.Shit
        exclude = ('slug', 'user')

class Comment(ModelForm):
    class Meta:
        model = M.Comment
        include = ('text', )
        exclude = ('shit', 'user')

class Review(ModelForm):
    class Meta:
        model = M.Review
        exclude = ('datetime', 'user')

class Image(ModelForm):
    class Meta:
        model = M.Image
        include = ('pic', )
        exclude = ('datetime', 'user')
