from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models

import datetime as dt

class Shit(models.Model):
    header = models.CharField(max_length=200)
    description = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    slug = models.SlugField()

    class Meta:
        unique_together = (('user', 'slug'), )
        ordering = ('-datetime', )

    def __unicode__(self):
        return self.header

class Comment(models.Model):
    shit = models.ForeignKey(Shit)
    user = models.ForeignKey(User)
    text = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        ordering = ('datetime', )
        
    def __unicode__(self):
        return self.text
    
class Visit(models.Model):
    shit = models.ForeignKey(Shit)
    user = models.ForeignKey(User)
    datetime = models.DateTimeField(auto_now_add=True)
    
    def update_now(self):
        self.datetime = dt.datetime.now()
        self.save()

class ShitDay(models.Model):
    user = models.ForeignKey(User)
    floated_timedelta = models.FloatField()
    next_shit = models.DateTimeField()

    def get_timedelta(self):
        return dt.timedelta(self.floated_timedelta)

    def set_timedelta(self, timedelta):
        self.floated_timedelta = timedelta.days + timedelta.seconds/86400.

    def get_next_shit_utc(self):
        return self.next_shit.strftime('%m/%d/%Y %I:%M %p %z')

    def __unicode__(self):
        return self.user
        
class Review(models.Model):
    user = models.ForeignKey(User)
    opinion = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s: %s'%(self.user, self.opinion[:80])

HINTS = (
    (u'comment', u'You can comment on a message using the form below.'),
    (u'reply', u'You can reply to user using "reply" link, also you can reply to twitter.'),
    (u'shithappens', u'If things are bad you can share with your friends by clicking the "Shit Happens" button.'),
)

class HintDelivery(models.Model):
    user = models.ForeignKey(User)
    hint = models.CharField(max_length=50, choices=HINTS)

    @classmethod
    def deliver(cls, request, label):
        hint_display = None
        for hint_label, hint in HINTS:
            if label==hint_label:
                hint_display = hint
                break
        if not hint_display:
            raise NameError, "Hint label \"%s\" not found"%label

        if request.user.is_authenticated():
            hint, created  = cls.objects.get_or_create(user=request.user, hint=label)
            if created:
                return hint_display
            else:
                return None
        else:
            if request.session.get('hint_%s'%label):
                return None
            else:
                request.session['hint_%s'%label] = True
                return hint_display

    def __unicode__(self):
        return self.hint

class Image(models.Model):
    user = models.ForeignKey(User)
    datetime = models.DateTimeField(auto_now_add=True)
    pic = models.ImageField(upload_to="uploaded_pics")

    def __unicode__(self):
        return u"Pic: <%s> at %s"%(self.user, self.datetime)
    
class ShitAdmin(admin.ModelAdmin):
    pass

class CommentAdmin(admin.ModelAdmin):
    pass

class ReviewAdmin(admin.ModelAdmin):
    pass

class ImageAdmin(admin.ModelAdmin):
    pass

admin.site.register(Shit, ShitAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Image, ImageAdmin)
