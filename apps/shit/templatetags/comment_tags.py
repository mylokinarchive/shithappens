from django.template import Library
from django.template.defaultfilters import stringfilter
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
import re

register = Library()

@register.filter
@stringfilter
def users_highlight(value):
    user_pattern = re.compile(r'@(?P<username>\w+)')
    found_users = list(set(user_pattern.findall(value)))
    for username in found_users:
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            continue
        usershit_link = reverse('shit:usershit', kwargs={'user':username})
        replacement = u"<a href=\"%s\">@%s</a>"%(usershit_link, username)
        value = value.replace('@%s'%username, replacement)
    return mark_safe(value)
