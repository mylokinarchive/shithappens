from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

from docutils.writers import html4css1
from docutils.core import publish_parts

register = Library()

@register.filter
@stringfilter
def rst2html(value):
    result = publish_parts(value, writer=html4css1.Writer())    
    return mark_safe(result['body'])
