from django.conf.urls.defaults import *

urlpatterns = patterns('shit.views',
    url(r'^about/$', 'about', name='about'),
    url(r'^happens/$', 'shithappens', name='shithappens'),
    url(r'^happens/submit/$', 'shithappens_submit', name='shithappens_submit'),
    url(r'^review/$', 'review', name='review'),
    url(r'^review/submit/$', 'review_submit', name='review_submit'),
    url(r'^pic/add/', 'pic_add', name='pic_add'),
    url(r'^pic/form/', 'pic_form', name='pic_form'),
    url(r'^(?P<user>.+)/(?P<slug>.+)/$', 'shit', name='shit'),
    url(r'^(?P<user>.+)/(?P<slug>.+)/comment$', 'comment', name='comment'),
    url(r'^(?P<user>.+)/$', 'usershit', name='usershit'),
)
