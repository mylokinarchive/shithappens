from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib import messages
from django.core.mail import mail_admins
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.db.models import Count
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import Context
from django.template import RequestContext
from django.template.loader import get_template

import settings
import forms as F
import models as M

from pytils.translit import slugify
from tweepy import TweepError
import urlparse
import datetime

def index(request):
    shits = M.Shit.objects.annotate(Count('comment')).order_by('-datetime', '-comment__count', 'header')
    messages.info(request, M.HintDelivery.deliver(request, u'shithappens'))
    context = RequestContext(request, {'shits':shits})
    return render_to_response('index.html', context)

def shit(request, user, slug):
    user = get_object_or_404(User, username=user)
    shit = get_object_or_404(M.Shit, user=user, slug=slug)
    form = F.Comment()
    comments = M.Comment.objects.filter(shit=shit).all()
    if request.user.is_authenticated():
        visit, created = M.Visit.objects.get_or_create(user=request.user, shit=shit)
        visit_datetime = visit.datetime
        new_comments = comments.filter(datetime__gte=visit_datetime).count()
        visit.update_now()
    else:
        visit_datetime = new_comments = None
    try:
        shitday = M.ShitDay.objects.get(user=user)
    except M.ShitDay.DoesNotExist:
        shitday = None
        
    hint = M.HintDelivery.deliver(request, u'comment') or \
           M.HintDelivery.deliver(request, u'reply')
    messages.info(request, hint)
    context = RequestContext(request, {'comments':comments, 'shit':shit, 'form':form, \
        'usershit':user, 'visit_datetime':visit_datetime, 'new_comments':new_comments, \
        'shitday':shitday})
    return render_to_response('shit/shit.html', context)

def shit_short_redirect(request, shit_id):
    shit = get_object_or_404(M.Shit, id=shit_id)
    path = reverse('shit:shit', \
        kwargs={'user':shit.user.username, 'slug':shit.slug})
    path = 'http://journalofshit.ru%s'%path
    return HttpResponseRedirect(path)

def usershit(request, user):
    user = get_object_or_404(User, username=user)
    shits = M.Shit.objects.filter(user=user)
    try:
        shitday = M.ShitDay.objects.get(user=user)
    except M.ShitDay.DoesNotExist:
        shitday = None
    context = RequestContext(request, {'shits':shits, 'shitday':shitday, 
        'usershit':user})
    return render_to_response('shit/usershit.html', context)

def comment(request, user, slug):
    user = get_object_or_404(User, username=user)
    shit = get_object_or_404(M.Shit, user=user, slug=slug)
    form = F.Comment(request.POST)
    if form.is_valid():
        if request.user.is_authenticated():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.shit = shit
            comment.save()
            messages.info(request, 'Thank you for your vote.')
            visit, created = M.Visit.objects.get_or_create(user=request.user, shit=shit)
            visit.update_now()
            if request.POST.get('twitter'):
                api = bitly.Api(login='mylokin', apikey='R_f6000ab74f4a8bdc0fa553761c4a63f8')
                path = reverse('shit:shit', kwargs={'user':request.user.username, \
                    'slug':comment.shit.slug})
                link = api.shorten(absurl(path))
                message = "%s %s"%(cut(comment.text, 120-len(link)), link)
                try:
                    request.user.twitter.api.update_status(message)
                except:
                    pass
            return HttpResponseRedirect(reverse('shit:shit', \
                kwargs={'user':user.username, 'slug':shit.slug}))
        else:
            request.session['twitter_post_data'] = dict(form.cleaned_data)
            return HttpResponseRedirect("%s?next=%s"%(settings.LOGIN_URL, request.path))
    else:
        comments = M.Comment.objects.filter(shit=shit).all()
        context = RequestContext(request, {'comments':comments, 'shit':shit, 'form':form})
        return render_to_response('shit/shit.html', context)
        


@login_required
def shithappens(request):
    usershit = request.user
    context = RequestContext(request, {'form':F.ShitHappens(), \
        'usershit':usershit})
    return render_to_response('shit/shithappens.html', context)

@login_required
def shithappens_submit(request):
    form = F.ShitHappens(request.POST)
    if form.is_valid():
        shit = form.save(commit=False)
        shit.user = request.user
        shit.slug = slugify(shit.header)
        path = reverse('shit:shit', kwargs={'user':request.user.username, 'slug':shit.slug})
        try:
            shit.save()
        except IntegrityError:
            messages.error(request, 'You have already talked about this here.')
        else:
            if request.POST.get('twitter'):
                link = 'http://jos.su/%s'%shit.id
                message = "#journalofshit %s %s"%(cut(shit.header, 120-len(link)), link)
                try:
                    request.user.twitter.api.update_status(message)
                except TweepError:
                    pass
                except:
                    pass
            usershit = M.Shit.objects.filter(user=request.user).order_by('-datetime')
            if usershit.count()>1:
                current_timedelta = usershit[0].datetime - usershit[1].datetime
                try:
                    shitday = M.ShitDay.objects.get(user=request.user)
                except M.ShitDay.DoesNotExist:
                    shitday = M.ShitDay()
                    shitday.user = request.user
                    shitday.set_timedelta(current_timedelta)
                    shitday.next_shit = datetime.datetime.now() + current_timedelta
                    shitday.save()
                else:
                    usershit_counter = usershit.count()
                    new_timedelta = (current_timedelta + shitday.get_timedelta()*(usershit_counter-1))
                    new_timedelta/=usershit_counter
                    shitday.set_timedelta(new_timedelta)
                    shitday.next_shit = datetime.datetime.now() + new_timedelta
                    shitday.save()
        return HttpResponseRedirect(path)
    else:
        context = RequestContext(request, {'form':form})
        return render_to_response('shit/shithappens.html', context)

def about(request):
    context = RequestContext(request)
    return render_to_response('shit/about.html', context)

@login_required
def review(request):
    usershit = request.user
    context = RequestContext(request, {'form':F.Review(), \
        'usershit':usershit})
    return render_to_response('shit/review.html', context)

@login_required
def review_submit(request):
    form = F.Review(request.POST)
    if form.is_valid():
        review = form.save(commit=False)
        review.user = request.user
        review.save()
        if settings.EMAIL_HOST:
            mail_admins(u'Review from %s'%review.user, review.opinion)
        context = RequestContext(request)
        return render_to_response('shit/review_thanks.html', context)
    else:
        context = RequestContext(request, {'form':form})
        return render_to_response('shit/review.html', context)

@login_required
def pic_add(request):
    if request.method=="POST":
        form = F.Image(request.POST, request.FILES)
        if form.is_valid():
            image = form.save(commit=False)
            image.user = request.user
            image.save()
            status = True if image else False
        else:
            status = False
        return render_to_response('shit/addpic.html', {'status':status})
    else:
        return HttpResponseForbidden("Forbidden")

@login_required
def pic_form(request):
    form = F.Image()
    context = RequestContext(request, {'form':form})
    return render_to_response('shit/addpicform.html', context)


def cut(string, length):
    length = int(length)
    gen = (word for word in string.split())
    string = gen.next()
    if len(string)>length:
        return '%s...'%string[:length]
    for word in gen:
        if len('%s %s'%(string, word))>length:
            return '%s...'%string
        else:
            string = '%s %s'%(string, word)
    return string

def absurl(path):
    domain = "http://journalofshit.ru" #TODO site bugfix
    return urlparse.urljoin(domain, path)    

def format_timedelta(timedelta):
    return str(timedelta).split('.', 2)[0]

def get_notification(user, filename, repeate=False, **kwargs):
    '''
    notification = get_notification(request.user, 'notification/new_comments.html', \
        new_comments_count=3)
    '''
    notification, created = M.NotificationDelivery.objects.get_or_create(user=user)
    if repeate or created:
        context = Context(kwargs)
        template = get_template(filename)
        return template.render(context)
    else:
        return None



