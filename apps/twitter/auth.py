from django.contrib.auth.models import User

import models as M

class Backend:
    def authenticate(self, twitter=None):
        if not twitter or not twitter.connected:
            return None
        twitter_username = twitter.api.me().screen_name
        user, user_created = User.objects.get_or_create(username=twitter_username)
        try:
            twitter_account_alias = M.TwitterAccount.objects.get(user=user)
        except M.TwitterAccount.DoesNotExist:
            twitter_account_alias = M.TwitterAccount()
            twitter_account_alias.user = user
            twitter_account_alias.twitter_username = twitter_username
            twitter_account_alias.access_token_key = twitter.auth.access_token.key
            twitter_account_alias.access_token_secret = twitter.auth.access_token.secret
            twitter_account_alias.save()
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except:
            return None
