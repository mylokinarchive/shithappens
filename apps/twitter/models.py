from django.contrib.auth.models import User
from django.db import models

from views import twitter

class TwitterAccount(models.Model):
    user = models.OneToOneField(User, related_name='twitter')
    twitter_username = models.CharField(max_length=50)#TODO paste truly max_length
    access_token_key = models.CharField(max_length=50)
    access_token_secret = models.CharField(max_length=50)

    class Meta:
        unique_together = (('user', 'twitter_username'), )
    
    @property
    def api(self):
        twitter.recall(self.access_token_key, self.access_token_secret)
        return twitter.api

