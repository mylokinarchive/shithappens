from django.conf.urls.defaults import *

urlpatterns = patterns('twitter.views',
    url(r'^login/$', 'twitter_login', name='login'),
    url(r'^logedin/$', 'twitter_logedin', name='logedin'),
    url(r'^logout/$', 'twitter_logout', name='logout'),
)
