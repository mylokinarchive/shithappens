from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
import tweepy

import settings

LOGIN_SUCCESSFUL = getattr(settings, 'LOGIN_REDIRECT_URL', '/')
LOGIN_FAILURE = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

def twitter_login(request):
    twitter_next_link = request.POST.get('next', request.GET.get('next'))
    if twitter_next_link:
        request.session['twitter_next_link'] = twitter_next_link
    return HttpResponseRedirect(twitter.authorization_url)    

def twitter_logedin(request):
    connected = twitter.connect(request)
    if connected:
        user = authenticate(twitter=twitter)
        login(request, user)
        twitter_next_link = request.session.get('twitter_next_link')
        if twitter_next_link:
            del request.session['twitter_next_link']
        twitter_post_data = request.session.get('twitter_post_data')
        if twitter_post_data:
            context = RequestContext(request, {'url':twitter_next_link, \
                'params':request.session['twitter_post_data']})
            del request.session['twitter_post_data']
            return render_to_response('post.html', context)
        return HttpResponseRedirect(twitter_next_link or LOGIN_SUCCESSFUL)
    else:
        return HttpResponseRedirect(LOGIN_FAILURE)

def twitter_logout(request):
    logout(request)
    request.session.flush()
    response = HttpResponseRedirect('/')
    response.delete_cookie('twitter.com')
    return response
    

class Twitter(object):
    consumer_key = getattr(settings, 'TWITTER_CONSUMER_KEY', None)
    consumer_secret = getattr(settings, 'TWITTER_CONSUMER_SECRET', None)
    callback_url = getattr(settings, 'TWITTER_CALLBACK_URL', None)


    def __init__(self):
        if not (self.consumer_key and self.consumer_secret and self.callback_url):
            raise TwitterInitFail
        self.auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret, self.callback_url)
        self.api = None
    
    @property
    def authorization_url(self):
        return self.auth.get_authorization_url()
        
    @property
    def connected(self):
        return True if self.api else False

    def access(self, oauth_verifier):
        try:
            self.auth.get_access_token(oauth_verifier)
        except:
            return False
        self.api = tweepy.API(self.auth)
        return True

    def connect(self, request):
        if request.GET.get('denied', False):
            return False
        oauth_verifier = request.GET.get('oauth_verifier', None)
        if oauth_verifier and self.access(oauth_verifier):
            return True
        else:
            return False

    def recall(self, access_token_key, access_token_secret):
        try:
            self.auth.set_access_token(access_token_key, access_token_secret)
        except:
            return False
        self.api = tweepy.API(self.auth)
        return True
    
class TwitterInitFail(Exception):
    pass

twitter = Twitter()
