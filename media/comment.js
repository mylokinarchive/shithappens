function setCaretPosition(ctrl, pos){
    if(ctrl.setSelectionRange){
	ctrl.focus();
	ctrl.setSelectionRange(pos,pos);
    } else if (ctrl.createTextRange) {
	var range = ctrl.createTextRange();
	range.collapse(true);
	range.moveEnd('character', pos);
	range.moveStart('character', pos);
	range.select();
    }
}

function reply_to(username){
    var el = document.forms.comment;
    var reply_to = "@"+username+" ";
    if(el.text.value){
	reply_to = "\n"+reply_to;
    }
    el.text.value += reply_to;
    setCaretPosition(el.text, el.text.length);
    el.text.focus();
}

childNodesIndexes = [1, 3];

function show_links(node){
    for (index in childNodesIndexes){
	childNode = node.childNodes[childNodesIndexes[index]];
	if(childNode.className=="comment-link"){
	    childNode.style.visibility = 'visible';
	}
    }
}

function hide_links(node){
    for (index in childNodesIndexes){
	childNode = node.childNodes[childNodesIndexes[index]];
	if(childNode.className=="comment-link"){
	    childNode.style.visibility = 'hidden';
	}
    }
}


		  