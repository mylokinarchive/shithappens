import os, sys

PRJ_ROOT = os.path.realpath(os.path.dirname(__file__))
project_root = lambda name: os.path.join(PRJ_ROOT, name)

sys.path.extend(map(project_root, ['apps', 'compat']))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Andrey Gubarev', 'noonerus@gmail.com'),
)

MANAGERS = ADMINS


EMAIL_HOST = 'mail.bz8.ru'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'mylokin_3'
EMAIL_HOST_PASSWORD = 'prW1tJMeHt2'
SERVER_EMAIL = 'review@journalofshit.ru'

DATABASES = {
    'default': {               
        'ENGINE': 'mysql',
        'NAME': 'db_mylokin_2',
        'USER': 'dbu_mylokin_2',
        'PASSWORD': 'ZlUy3iC6oPX',
        'HOST': 'mysql.mylokin.cz8.ru',
        'PORT': '',
    }
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = project_root('media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'jf2f%1odn0*-z5yno3zgj*6^f16$3252)wijasz%)7p@i#qwv#'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

FILE_UPLOAD_HANDLERS = (
    'django.core.files.uploadhandler.MemoryFileUploadHandler',
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)

ROOT_URLCONF = 'shithappens.urls'

TEMPLATE_DIRS = (
    project_root('templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
     'django.core.context_processors.auth',
     'django.core.context_processors.debug',
     'django.core.context_processors.i18n',
     'django.core.context_processors.media',
     'django.core.context_processors.request',
     'django.contrib.messages.context_processors.messages',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    
    'twitter',
    'shit',
)

LOGIN_URL = '/twitter/login'
LOGIN_REDIRECT_URL = '/'

#Twitter application configuration
AUTHENTICATION_BACKENDS = (
    'twitter.auth.Backend', 
    'django.contrib.auth.backends.ModelBackend',
)

TWITTER_CONSUMER_KEY = "jSPv4QgOnermgFuFWF5oA"
TWITTER_CONSUMER_SECRET = "Cx9X8GmW2wNwLftlNRSuKoYrf1Y3tYNkSC0WMTncQ"
TWITTER_CALLBACK_URL = "http://journalofshit.ru/webapp/twitter/logedin"

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'


try:
    from settings_local import *
except ImportError:
    pass

