from django.conf.urls.defaults import *
from django.contrib import admin
import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'shit.views.index'),
    url(r'^(?P<shit_id>\d+)$', 'shit.views.shit_short_redirect'),
    (r'^shit/', include('shit.urls', namespace='shit', app_name='shit')),
    (r'^twitter/', include('twitter.urls', namespace='twitter', app_name='twitter')),
    (r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('django.views',
        url(r'^'+settings.MEDIA_URL[1:]+'(.*)$',
            'static.serve',
            dict(document_root=settings.MEDIA_ROOT)
        ),
    )
